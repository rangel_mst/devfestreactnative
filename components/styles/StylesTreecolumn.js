import { Platform, StyleSheet, StatusBar } from 'react-native';

// const styles = StyleSheet.create({
//   container: {
//   flex: 1,
//   justifyContent: 'center',
//   alignItems: 'center',
//   backgroundColor: 'ghostwhite',
//     ...Platform.select({
//       ios: { paddingTop: 20 },
//       android: { paddingTop: StatusBar.currentHeight }
//     })
//   },
//
//   box: {
//     width: 500,
//     height: 100,
//     justifyContent: 'center',
//     alignItems: 'center',
//     backgroundColor: 'lightgray'
//   },
//
//   boxText: {
//   color: 'darkslategray',
//   fontWeight: 'bold'
//   }
// });

const styles = StyleSheet.create({
container: {
  flex: 1,
  flexDirection: 'column',
  justifyContent: 'space-around',
  alignItems: 'center',
  backgroundColor: 'ghostwhite',
  ...Platform.select({
  ios: { paddingTop: 20 },
  android: { paddingTop: StatusBar.currentHeight }
  })
},

  box: {
    height: 100,
    justifyContent: 'center',
    // Instead of given the flexbox child a width, we
    // tell it to "stretch" to fill all available space.
    alignSelf: 'stretch',
    alignItems: 'center',
    backgroundColor: 'lightgray',
    borderWidth: 1,
    borderStyle: 'dashed',
    borderColor: 'darkslategray'
  },

  boxText: {
    color: 'darkslategray',
    fontWeight: 'bold'
  }
});

export default styles;
