import { Platform, StyleSheet, StatusBar } from 'react-native';
// Exports a "stylesheet" that can be used
// by React Native components. The structure is
// familiar for CSS authors.

const styles = StyleSheet.create({
  container: {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: 'ghostwhite',
  ...Platform.select({
  ios: { paddingTop: 20 },
  android: { paddingTop: StatusBar.currentHeight }
  })
},

box: {
  width: 100,
  height: 100,
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: 'lightgray'
},

boxText: {
  color: 'darkslategray',
  fontWeight: 'bold'
},

fieldText:{
  fontSize: 20,
  fontWeight: 'bold',
},

//Estilo para cuadros de textInputField
textInputField:{
  width:250,
  height:50,
  fontSize:24
},
//Estilo para botones
buttonStyle1:{
  width:100,
  height:50,
}
,
//Estilo para FlatList
item: {
       margin: 5,
       padding: 5,
       color: 'slategrey',
       backgroundColor: 'ghostwhite',
       textAlign: 'center',
},

});

export default styles;
