// import React from 'react';
// import { Platform, StatusBar, StyleSheet, View } from 'react-native';
// import { AppLoading, Asset, Font, Icon } from 'expo';
// import AppNavigator from './navigation/AppNavigator';
// import { AppRegistry, StyleSheet, Text, View } from 'react-native';

// export default class App extends React.Component {
//   state = {
//     isLoadingComplete: false,
//   };
//
//   render() {
//     if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
//       return (
//         <AppLoading
//           startAsync={this._loadResourcesAsync}
//           onError={this._handleLoadingError}
//           onFinish={this._handleFinishLoading}
//         />
//       );
//     } else {
//       return (
//         <View style={styles.container}>
//           {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
//           <AppNavigator />
//         </View>
//       );
//     }
//   }
//
//   _loadResourcesAsync = async () => {
//     return Promise.all([
//       Asset.loadAsync([
//         require('./assets/images/robot-dev.png'),
//         require('./assets/images/robot-prod.png'),
//       ]),
//       Font.loadAsync({
//         // This is the font that we are using for our tab bar
//         ...Icon.Ionicons.font,
//         // We include SpaceMono because we use it in HomeScreen.js. Feel free
//         // to remove this if you are not using it in your app
//         'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
//       }),
//     ]);
//   };
//
//   _handleLoadingError = error => {
//     // In this case, you might want to report the error to your error
//     // reporting service, for example Sentry
//     console.warn(error);
//   };
//
//   _handleFinishLoading = () => {
//     this.setState({ isLoadingComplete: true });
//   };
// }
//
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//   },
// });

//_______________________Ejemplo de Platform Specific____________________
// import React, { Component } from 'react';
// import { Platform,AppRegistry, StyleSheet, Text, View } from 'react-native';
//
// export default class LotsOfStyles extends Component {
//   render() {
//     return (
//       <View style={styles.container}>
//         <Text style={styles.myText}>just red</Text>
//       </View>
//     );
//   }
// }
//
// //Estilos de Ejemplo
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     ...Platform.select({
//       ios: {
//         backgroundColor: 'red',
//       },
//       android: {
//         backgroundColor: 'blue',
//       },
//     }),
//   },
//
//   myText:{
//     fontSize: 50,
//   }
// });
//_______________________///////////////////////////////____________________

//_______________________Ejemplo de Estilos 1____________________
//                    **** I'm a Box example ****
// import React from 'react';
// import {Text,View} from 'react-native';
//
// import styles from './components/styles/styles';
// export default () => (
//   <View style={styles.container}>
//   <View style={styles.box}>
//   <Text style = {styles.boxText}>I'm in a box</Text>
//   </View>
//   </View>
// );

//_______________________///////////////////////////////____________________

//_______________________Ejemplo de Estilos 2____________________
//              **** Simple three column layout *****
// import React from 'react';
// import {Text,View} from 'react-native';
// import styles from './components/styles/StylesTreecolumn';
//
// export default () => (
//   <View style = {styles.container}>
//     <View style = {styles.box}>
//       <Text style = {styles.boxText}>#1</Text>
//     </View>
//     <View style = {styles.box}>
//       <Text style = {styles.boxText}>#2</Text>
//     </View>
//     <View style = {styles.box}>
//       <Text style = {styles.boxText}>#2</Text>
//     </View>
//   </View>
// );

//_______________________///////////////////////////////____________________

//_______________________Ejemplo de Estilos 3____________________
//                      **** Flexible Rows ****
// import React from 'react';
// import { Text, View, StatusBar } from 'react-native';
// import styles from './components/styles/StyleFlexibleRows';
// import Box from './components/exampleComponents/boxExample';
//
// export default () => (
//   <View style = {styles.container}>
//   <Box>#1</Box>
//   <Box>#2</Box>
//   </View>
// );

//_______________________///////////////////////////////____________________

//_______________________Ejemplo de Estilos 4____________________
//                      **** Flexible Grids ****
// **** Flexible
// import React from 'react';
// import { View, StatusBar } from 'react-native';
//
// import styles from './components/styles/StyleFlexibleGrids';
// import Box from './components/exampleComponents/boxExample';
//
// // An array of 10 numbers, representing the grid
// // sections to render.
// const boxes = new Array(10).fill(null).map((v, i) => i + 1);
//
// export default () => (
//   <View style={styles.container}>
//   <StatusBar hidden={false} />
//   {/* Renders 10 "<Box>" sections */}
//   {
//     boxes.map(i => <Box key={i}>#{i}</Box>)
//   }
//   </View>
// );
//_______________________///////////////////////////////____________________

//_______________________Ejemplo de Estilos 5____________________
//                  **** Flexible Rows and Columns *****
// import React from 'react';
// import { View, StatusBar } from 'react-native';
// import styles from './components/styles/styles';
// import Row from './components/styles/StyleflexibleGrids';
// import Column from './components/styles/StylesTreecolumn';
// import Box from './components/exampleComponents/boxExample';
//
// export default () => (
// <View style={styles.container}>
// <StatusBar hidden={false} />
// {/* This row contains two columns. The first column
// has boxes "#1" and "#2". They will be stacked on
// top of one another. The next column has boxes "#3"
// and "#4", which are also stacked on top of one
// another */}
// <Row>
//
// </Row>
// </View>
// );
//_______________________///////////////////////////////____________________

//_______________________Ejemplo Practico 1____________________
//                      **** Route Parameters ****
// import { createStackNavigator } from 'react-navigation';
// import Home from './navigation/exampleNavigation/Home';
// //import Settings from './navigation/exampleNavigation/Settings';
// import Details from './navigation/exampleNavigation/Details';
//
// export default createStackNavigator(
// {
//   Home,
//   Details
//   },
//   { initialRouteName: 'Home' }
// );
//_______________________///////////////////////////////____________________

//_______________________Ejemplo Practico 2____________________
//                **** Tab and Drawer navigation ****
// import {
// createBottomTabNavigator,
// createDrawerNavigator
// } from 'react-navigation';
//
// import { Platform } from 'react-native';
// import Home from './navigation/exampleNavigation/Home';
// import News from './navigation/exampleNavigation/News';
// import Settings from './navigation/exampleNavigation/Settings';
//
// const { createNavigator } = Platform.select({
// ios: { createNavigator: createBottomTabNavigator },
// android: { createNavigator: createDrawerNavigator }
// });
// export default createNavigator(
// {
//   Home,
//   News,
//   Settings
// },
// { initialRouteName: 'Home' }
// );

//_______________________Ejemplo Practico 3____________________
//                      **** Handling State ****
// import React, { Component } from 'react';
// import { createStackNavigator } from 'react-navigation';
// import Home from './navigation/exampleNavigation/Home';
// import Details from './navigation/exampleNavigation/Details';
//
// const Nav = createStackNavigator(
// {
//   Home,
//   Details
//   },
//   { initialRouteName: 'Home' }
// );
//
// export default class App extends Component {
// state = {
//   stock: {
//   first: 1,
//   second: 0,
//   third: 200
//   }
// };
//
// updateStock = id => {
//   this.setState(({ stock }) => ({
//   stock: {
//   ...stock,
//   [id]: stock[id] === 0 ? 0 : stock[id] - 1
//   }
//   }));
// };
//
// render() {
//   const props = {
//   ...this.state,
//   updateStock: this.updateStock
//   };
//   return <Nav screenProps={props} />;
// }
// }

//_______________________///////////////////////////////____________________

//_______________________Ejemplo Practico 4____________________
//            *** Renderizar listas con arreglos ***
// import React from 'react';
// import { Text, View, FlatList } from 'react-native';
// import styles from './components/styles/styles';
//
// const data = new Array(100)
// .fill(null)
// .map((v, i) => ({ key: i.toString(), value: `Item ${i}` }));
//
// export default () => (
// <View style={styles.container}>
//   <FlatList
//   data={data}
//   renderItem={({ item }) => (
//     <Text style={styles.item}>{item.value}</Text>
//   )}
//   />
// </View>
// );
//_______________________///////////////////////////////____________________



//_______________________________________




// _______________________Ejemplo Exposicion (Flexbox Direction)____________________
//
// import React, { Component } from 'react';
// import { AppRegistry, View } from 'react-native';
//
// export default class FlexDirectionBasics extends Component {
//   render() {
//     return (
//       // Try setting `flexDirection` to `column`.
//       <View style={{flex: 1, flexDirection: 'column'}}>
//         <View style={{width: 50, height: 50, backgroundColor: 'powderblue'}} />
//         <View style={{width: 50, height: 50, backgroundColor: 'skyblue'}} />
//         <View style={{width: 50, height: 50, backgroundColor: 'steelblue'}} />
//       </View>
//     );
//   }
// };
// _______________________///////////////////////////////____________________
// _______________________Ejemplo Exposicion (Flexbox justifyContent)____________________

// import React, { Component } from 'react';
// import { AppRegistry, View } from 'react-native';
//
// export default class JustifyContentBasics extends Component {
//   render() {
//     return (
//       // Try setting `justifyContent` to `center`.
//       // Try setting `flexDirection` to `row`.
//       <View style={{
//         flex: 1,
//         flexDirection: 'column',
//         // justifyContent: 'flex-start',
//         // justifyContent: 'center',
//         // justifyContent: 'flex-end',
//         // justifyContent: 'space-around',
//         // justifyContent: 'space-between',
//         justifyContent: 'space-evenly',
//
//
//       }}>
//         <View style={{width: 50, height: 50, backgroundColor: 'powderblue'}} />
//         <View style={{width: 50, height: 50, backgroundColor: 'skyblue'}} />
//         <View style={{width: 50, height: 50, backgroundColor: 'steelblue'}} />
//       </View>
//     );
//   }
// };
// _______________________///////////////////////////////____________________

//_______________________Ejemplo Exposicion____________________
import {
  createBottomTabNavigator,
  createDrawerNavigator,
  createMaterialTopTabNavigator
} from 'react-navigation';

import { Platform } from 'react-native';

//Archivos de rutas
import Calculadora from './navigation/expoNavigation/Calculadora';
import Listas from './navigation/expoNavigation/Listas';
import Progreso from './navigation/expoNavigation/Progreso';

//Se verifica la seleccion de la plataforma y que tipo de navegador se utilizara
const { createNavigator } = Platform.select({
ios: { createNavigator: createMaterialTopTabNavigator },
android: { createNavigator: createDrawerNavigator }
});

export default createNavigator(
  {
    Calculadora,
    Listas,
    Progreso,
  },
  {initialRouteName: 'Calculadora'}
);
//_______________________///////////////////////////////____________________

// import React, { Component } from 'react';
// import { Platform,AppRegistry, StyleSheet, Text, View } from 'react-native';
//
// export default class LotsOfStyles extends Component {
//   render() {
//     return (
//       <View style={styles.container}>
//         <Text style={styles.myText}>just red</Text>
//       </View>
//     );
//   }
// }
//
// //Estilos de Ejemplo
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     ...Platform.select({
//       ios: {
//         backgroundColor: 'red',
//       },
//       android: {
//         backgroundColor: 'blue',
//       },
//     }),
//   },
//
//   myText:{
//     fontSize: 50,
//   }
// });
