import React ,{Component} from 'react';
import {View,ActivityIndicator,Text,Button} from 'react-native';
import styles from '../../components/styles/styles';

export default class Progreso extends Component{
  constructor(props){
    super(props);
    this.state = {
      mensaje: 'Ejemplo de Indicador de Actividad',
      titulo: 'Presiona el boton para iniciar el proceso',
      isLoading: false,
    };
  }
  
  setProcess = () => {
    this.setState({isLoading: true})
  }

  render() {

    if(!this.state.isLoading)
    {
      return (
        <View style={styles.container}>
          <Text style={styles.fieldText}>
              {this.state.titulo}
          </Text>
          <Button
            onPress = { () => {
              this.setProcess();
            }}
          title='Iniciar'
          />
        </View>
      );
    }
    else {

      return(
        <View style={styles.container}>
          <ActivityIndicator size="large"/>
          <Text style={styles.fieldText}>
              {this.state.mensaje}
          </Text>
        </View>
      );
    }
  }
}
