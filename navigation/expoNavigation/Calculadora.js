import React ,{Component} from 'react';
import { View, Text, Button, Alert, TextInput } from 'react-native';
import styles from '../../components/styles/styles';

export default class Calculadora extends Component {
  constructor(props){
    super(props);

    this.state = {
      resultado:'',
      num1:'',
      num2:''
    };

  }

  setNumer1 = (text) => {
     this.setState({ num1: text })
  }

  setNumer2 = (text) => {
     this.setState({ num2: text })
  }

  clearData = () => {
    this.setState({num1: ''})
    this.setState({num2: ''})
    this.setState({resultado:''})
  }

  render() {
    return (
      <View style={styles.container}>

      <TextInput
        style={styles.textInputField}
        placeholder="Numero 1"
        onChangeText={this.setNumer1}
        value = {this.state.num1}
      />

      <TextInput
        style={styles.textInputField}
        placeholder="Numero 2"
        onChangeText={this.setNumer2}
        value = {this.state.num2}
      />

      <Button
          onPress={() => {
            var num1 = parseInt(this.state.num1);
            var num2 = parseInt(this.state.num2);

            var suma = num1 + num2;
            this.setState({resultado: suma.toString()});
            //this.setResultado = suma.toString();
            //Alert.alert('Resultado: ' + suma);
          }}
         title="Sumar"
       />

       <TextInput
         style={styles.textInputField}
         value = {this.state.resultado}
         placeholder="Resultado"
       />

       <Button
          color = "red"
          onPress = { () => {
            this.clearData();
          }}
          title = "Limpiar"
       />

      </View>

    );
  }
}

Calculadora.navigationOptions = {
title: 'Calculadora'
};
