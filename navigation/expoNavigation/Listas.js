import React from 'react';
import { View, Text, FlatList } from 'react-native';
import styles from '../../components/styles/styles';

const data = new Array(2)
  .fill(null)
  .map((v,i) => ({key: i.toString(),value: `Item ${i}`}));

  export default () => (
    <View style = {styles.container}>
      <FlatList
      data={data}
      renderItem = {({item}) => (
        <Text style = {styles.item}>{item.value}</Text>
      )}
      />
    </View>
  );














  // const Parametros = ({ navigation }) => (
  //   <View style={styles.container}>
  //     <Text style={{fontSize:20,color:'red'}}>Parametros Content</Text>
  //   </View>
  // );
  //
  // Parametros.navigationOptions = {
  // title: 'Parametros'
  // };
  // export default Parametros;
