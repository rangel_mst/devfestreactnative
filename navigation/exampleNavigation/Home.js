import React from 'react';
import { View, Text, Button } from 'react-native';
import styles from '../../components/styles/styles';

// export default ({ navigation }) => (
// <View style={styles.container}>
//   <Text>Home Screen</Text>
//   <Button
//     title="First Item"
//     onPress={() =>
//     navigation.navigate('Details', { title: 'First Item' })
//     }
//   />
//
//   <Button
//     title="Second Item"
//     onPress={() =>
//     navigation.navigate('Details', { title: 'Second Item' })
//   }
//   />
//
//   <Button
//     title="Third Item"
//     onPress={() =>
//     navigation.navigate('Details', { title: 'Third Item' })
//     }
//   />
// </View>
// );


// const Home = ({ navigation }) => (
//   <View style={styles.container}>
//   <Button
//     title="First Item"
//     onPress={() =>
//       navigation.navigate('Details', {
//       title: 'First Item',
//       content: 'First Item Content',
//       stock: 1
//     })
//     }
//   />
//   <Button
//     title="Second Item"
//       onPress={() =>
//       navigation.navigate('Details', {
//       title: 'Second Item',
//       content: 'Second Item Content',
//       stock: 0
//     })
//     }
//   />
//   <Button
//     title="Third Item"
//       onPress={() =>
//       navigation.navigate('Details', {
//       title: 'Third Item',
//       content: 'Third Item Content',
//       stock: 200
//     })
//     }
//   />
//   </View>
// );
// Home.navigationOptions = {
//   title: 'Home'
// };
//
// export default Home;


// const Home = ({ navigation }) => (
//   <View style={styles.container}>
//     <Text>Home Content</Text>
//   </View>
// );
//
// Home.navigationOptions = {
//   title: 'Home'
// };
//
// export default Home;


const Home = ({ navigation, screenProps: { stock } }) => (
<View style={styles.container}>
<Button
  title={`First Item (${stock.first})`}
  onPress={() =>
  navigation.navigate('Details', {
    id: 'first',
    title: 'First Item',
    content: 'First Item Content'
  })
  }
/>

<Button
title={`Second Item (${stock.second})`}
onPress={() =>
  navigation.navigate('Details', {
  id: 'second',
  title: 'Second Item',
  content: 'Second Item Content'
  })
}
/>

<Button
title={`Third Item (${stock.third})`}
onPress={() =>
  navigation.navigate('Details', {
  id: 'third',
  title: 'Third Item',
  content: 'Third Item Content'
  })
  }
/>
</View>
);

Home.navigationOptions = {
  title: 'Home'
};
export default Home;
