import React from 'react';
import { View, Text, Button } from 'react-native';

import styles from '../../components/styles/styles';

// export default ({ navigation }) => (
// <View style={styles.container}>
//   <Text>Home Screen</Text>
//   <Button
//   title="First Item"
//   onPress={() =>
//   navigation.navigate('Details', { title: 'First Item' })
//   }
//   />
//   <Button
//   title="Second Item"
//   onPress={() =>
//   navigation.navigate('Details', { title: 'Second Item' })
//   }
//   />
//   <Button
//   title="Third Item"
//   onPress={() =>
//   navigation.navigate('Details', { title: 'Third Item' })
//   }
//   />
// </View>


  // export default ({ navigation }) => (
  // <View style={styles.container}>
  //   <Text>{navigation.getParam('title')}</Text>
  // </View>
  // );

  const Details = ({ navigation }) => (
  <View style={styles.container}>
    <Text>{navigation.getParam('content')}</Text>
  </View>
  );

  Details.navigationOptions = ({
  navigation,
  screenProps: { stock, updateStock }
  }) => {
  const id = navigation.getParam('id');
  const title = navigation.getParam('title');

  return {
    title,
    headerRight: (
    <Button
    title="Buy"
    onPress={() => updateStock(id)}
    disabled={stock[id] === 0}
    />
    )
    };
};

export default Details;
